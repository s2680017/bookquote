package nl.utwente.di.bookQuote;

import java.util.HashMap;
import java.util.Map;

public class Quoter {
    Map bookp = new HashMap<String, Double>();


    public Quoter() {
        bookp.put("1", 10.0);
        bookp.put("2", 45.0);
        bookp.put("3", 20.0);
        bookp.put("4", 35.0);
        bookp.put("5", 50.0);
    }

    public double  getBookPrice(String isbn) {
        if (bookp.containsKey(isbn))
            return  (double) bookp.get(isbn);
        return 0;
    }
}
